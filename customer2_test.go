package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)


func TestCustomer(t *testing.T) {
	req,err:=http.NewRequest("GET","/customers",nil)
	if err!=nil {
		t.Fatal(err)
	}
	recorder:=httptest.NewRecorder()
	handler := http.HandlerFunc(Customer)
	handler.ServeHTTP(recorder, req)
	if status := recorder.Code; status != http.StatusOK {
		t.Errorf("Wrong Status Code: got %v want %v",
			status, http.StatusOK)
	}

	expected:= `{"Name":"Akanksh","Ph_no":"9994289992","Age":20}
{"Name":"Akhil","Ph_no":"9912312389","Age":23}
{"Name":"John","Ph_no":"9869331238","Age":29}
{"Name":"Rock","Ph_no":"82313331238","Age":38}
`
	/*expected:=""
	db, _ := sql.Open("mysql",
		"root:password@tcp(127.0.0.1:3306)/hello")
	var (
		name string
		phno string
		age  int
	)
	rows, _ := db.Query("select * from users ")
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&name, &phno, &age)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(name)
		res := &listshow{
			Name: name,
			Phno: phno,
			Age:  age,}
		result, _ := json.Marshal(res)
		expected+=string(result)+"\n"
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	*/

	if recorder.Body.String() != expected {
		t.Errorf("Handler returned an unexpected body: got\n %v want\n %v",
			recorder.Body.String(), expected)
	}
}


func BenchmarkCustomer(b *testing.B) {
	req,err:=http.NewRequest("GET","/customers",nil)
	if err!=nil {
		b.Fatal(err)
	}
	recorder:=httptest.NewRecorder()
	handler := http.HandlerFunc(Customer)
	handler.ServeHTTP(recorder, req)
	expected:= `{"Name":"Akanksh","Ph_no":"9994289992","Age":20}
{"Name":"Akhil","Ph_no":"9912312389","Age":23}
{"Name":"John","Ph_no":"9869331238","Age":29}
{"Name":"Rock","Ph_no":"82313331238","Age":38}
`
	for i:=0;i<b.N;i++ {
		if recorder.Body.String() != expected {
			b.Errorf("handler returned unexpected body: got\n %v want\n %v",
				recorder.Body.String(), expected)
		}
	}
}
