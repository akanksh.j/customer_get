package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"net/http"
)


type ListOfAttributes struct {
	Name string
	Ph_no string
	Age int
}

func Customer(w http.ResponseWriter,r* http.Request) {

	if r.Method == http.MethodGet {
		db, err := sql.Open("mysql",
			"root:password@tcp(127.0.0.1:3306)/hello")
		if err != nil {
			log.Fatal(err)
		}
		var (
			name string
			phno string
			age  int
		)
		rows, err := db.Query("select * from users ")
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()
		for rows.Next() {
			err := rows.Scan(&name, &phno, &age)
			if err != nil {
				log.Fatal(err)
			}
			result := &ListOfAttributes{
				Name: name,
				Ph_no: phno,
				Age:  age,}
			result_JSON, _ := json.Marshal(result)
			fmt.Fprintf(w, "%s\n",(result_JSON))
		}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	} else	{

		fmt.Fprintf(w,"This is a POST request")
	}
}


func main() {
	//rtr := mux.NewRouter()
	//rtr.HandleFunc("/customer",Customer).Methods("GET")
	//http.Handle("/",rtr)

	http.HandleFunc("/customer",Customer)
	http.ListenAndServe(":8080",nil)
}

